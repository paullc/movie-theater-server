package com.paullc.MovieTheaterServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieTheaterServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieTheaterServerApplication.class, args);
	}

}
