package com.paullc.MovieTheaterServer.controller;


import com.paullc.MovieTheaterServer.dto.auditorium.AuditoriumCreationDto;
import com.paullc.MovieTheaterServer.dto.auditorium.AuditoriumResponseDto;
import com.paullc.MovieTheaterServer.model.Auditorium;
import com.paullc.MovieTheaterServer.service.AuditoriumService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
@RequestMapping("api/auditorium")
@CrossOrigin
@Valid
public class AuditoriumController {


    private final AuditoriumService auditoriumService;
    private final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public AuditoriumController(AuditoriumService auditoriumService) {
        this.auditoriumService = auditoriumService;
    }


    @PostMapping("/create")
    public ArrayList<AuditoriumResponseDto> createAuditoriums(@RequestBody @Valid AuditoriumCreationDto auditoriumCreationDto) {


        ArrayList<Auditorium> auditoriums = IntStream.range(0, auditoriumCreationDto.getQuantity())
                .mapToObj((i) -> this.auditoriumService.createAuditorium(new Auditorium()))
                .collect(Collectors.toCollection(ArrayList::new));

        return this.auditoriumService.createAuditoriums(auditoriums)
                .stream()
                .map((auditorium) -> this.convertToAuditoriumResponseDto(auditorium))
                .collect(Collectors.toCollection(ArrayList::new));

    }

    @GetMapping("/search")
    public ArrayList<AuditoriumResponseDto> searchAuditoriums(
            @RequestParam(value = "id", required = false) Integer id) {

        return this.auditoriumService.searchAuditoriums(id)
                .stream()
                .map((seat) -> this.convertToAuditoriumResponseDto(seat))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // Dto <-> Entity Conversions

    // Probably not needed
    private Auditorium convertToAuditoriumEntity(AuditoriumCreationDto auditoriumCreationDto) {
        Auditorium auditorium = modelMapper.map(auditoriumCreationDto, Auditorium.class);
        return auditorium;
    }


    private AuditoriumResponseDto convertToAuditoriumResponseDto(Auditorium auditorium) {
        AuditoriumResponseDto auditoriumResponseDto = modelMapper.map(auditorium, AuditoriumResponseDto.class);
        return auditoriumResponseDto;
    }

}
