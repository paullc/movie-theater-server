package com.paullc.MovieTheaterServer.controller;

import com.paullc.MovieTheaterServer.dto.film.FilmCreationDto;
import com.paullc.MovieTheaterServer.dto.film.FilmResponseDto;
import com.paullc.MovieTheaterServer.model.Film;
import com.paullc.MovieTheaterServer.service.FilmService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/film")
@Validated
@CrossOrigin
public class FilmController {

    private final FilmService filmService;
    private final ModelMapper modelMapper = new ModelMapper();


    @Autowired
    public FilmController(FilmService filmService) {
        this.filmService = filmService;
    }


    @PostMapping("/create")
    public ArrayList<FilmResponseDto> createFilms(@RequestBody @Valid ArrayList<FilmCreationDto> filmCreationDtos) {

        ArrayList<Film> films = filmCreationDtos
                .stream()
                .map((dto) -> this.convertToFilmEntity(dto))
                .collect(Collectors.toCollection(ArrayList::new));


        return this.filmService.createFilms(films).stream()
                .map((film) -> this.convertToFilmResponseDto(film))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @GetMapping("/search")
    public ArrayList<FilmResponseDto> searchFilms(
            @RequestParam(value = "id", required = false) Integer id,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "duration", required = false) Integer duration,
            @RequestParam(value = "director", required = false) String director,
            @RequestParam(value = "mpaaRating", required = false) String mpaaRating) {

        return this.filmService.searchFilms(id, title, duration, director, mpaaRating)
                .stream()
                .map((film) -> this.convertToFilmResponseDto(film))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // Dto <-> Entity Conversions

    private Film convertToFilmEntity(FilmCreationDto filmCreationDto) {
        Film film = modelMapper.map(filmCreationDto, Film.class);
        return film;
    }


    private FilmResponseDto convertToFilmResponseDto(Film film) {
        FilmResponseDto filmResponseDto = modelMapper.map(film, FilmResponseDto.class);
        return filmResponseDto;
    }
}
