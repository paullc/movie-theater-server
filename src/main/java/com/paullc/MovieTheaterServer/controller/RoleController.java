package com.paullc.MovieTheaterServer.controller;

import com.paullc.MovieTheaterServer.model.Role;
import com.paullc.MovieTheaterServer.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/role")
@Validated
@CrossOrigin
public class RoleController {

    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PostMapping(path = "/create")
    public Role createRole(@RequestBody @Valid Role role) {

        return this.roleService.createRole(role);
    }

    @GetMapping(path = "/search")
    public Iterable<Role> searchRoles(
            @RequestParam(value = "id", required = false) Integer id,
            @RequestParam(value = "name", required = false) String name) {

        return this.roleService.search(id, name);
    }

}
