package com.paullc.MovieTheaterServer.controller;

import com.paullc.MovieTheaterServer.dto.seat.SeatCreationDto;
import com.paullc.MovieTheaterServer.dto.seat.SeatDeletionDto;
import com.paullc.MovieTheaterServer.dto.seat.SeatResponseDto;
import com.paullc.MovieTheaterServer.model.Seat;
import com.paullc.MovieTheaterServer.service.AuditoriumService;
import com.paullc.MovieTheaterServer.service.SeatService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/seat")
@Valid
@CrossOrigin
public class SeatController {

    private final SeatService seatService;
    private final AuditoriumService auditoriumService;
    private final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public SeatController(SeatService seatService, AuditoriumService auditoriumService) {
        this.seatService = seatService;
        this.auditoriumService = auditoriumService;
    }


    @PostMapping("/create")
    public ArrayList<SeatResponseDto> createSeats(@RequestBody @Valid ArrayList<SeatCreationDto> seatCreationDtos) {

        ArrayList<Seat> seats = seatCreationDtos
                .stream()
                .map((dto) -> this.convertToSeatEntity(dto))
                .collect(Collectors.toCollection(ArrayList::new));


        return this.seatService.createSeats(seats).stream()
                .map((film) -> this.convertToSeatResponseDto(film))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @DeleteMapping("/delete")
    public void deleteSeats(@RequestBody @Valid ArrayList<SeatDeletionDto> seatDeletionDtos) {

        seatDeletionDtos.forEach((dto) -> this.seatService
                .deleteSeat(dto.getSeatRow(), dto.getSeatNumber(), dto.getAuditoriumId()));
    }

    @DeleteMapping("/delete/by-ids")
    public void deleteSeatsById(@RequestBody ArrayList<Integer> ids) {
        if (ids.isEmpty()) {
            return;
        }

        this.seatService.deleteSeatsById(ids);
    }


    @GetMapping("/search")
    public ArrayList<SeatResponseDto> searchSeats(
            @RequestParam(value = "id", required = false) Integer id,
            @RequestParam(value = "seatRow", required = false) String seatRow,
            @RequestParam(value = "seatNumber", required = false) Integer seatNumber,
            @RequestParam(value = "auditoriumId", required = false) Integer auditoriumId) {

        return this.seatService.searchSeats(id, seatRow, seatNumber, auditoriumId)
                .stream()
                .map((seat) -> this.convertToSeatResponseDto(seat))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // Dto <-> Entity Conversions

    private Seat convertToSeatEntity(SeatCreationDto seatCreationDto) {
        Seat seat = modelMapper.map(seatCreationDto, Seat.class);
        seat.setAuditorium(this.auditoriumService.findById(seatCreationDto.getAuditoriumId()));
        return seat;
    }


    private SeatResponseDto convertToSeatResponseDto(Seat seat) {
        SeatResponseDto seatResponseDto = modelMapper.map(seat, SeatResponseDto.class);
        seatResponseDto.setAuditoriumId(seat.getAuditorium().getId());
        return seatResponseDto;
    }

}
