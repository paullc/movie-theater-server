package com.paullc.MovieTheaterServer.controller;

import com.paullc.MovieTheaterServer.dto.user.UserCreationDto;
import com.paullc.MovieTheaterServer.dto.user.UserResponseDto;
import com.paullc.MovieTheaterServer.model.User;
import com.paullc.MovieTheaterServer.service.RoleService;
import com.paullc.MovieTheaterServer.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/user")
@Validated
@CrossOrigin
public class UserController {

    private final UserService userService;
    private final RoleService roleService;
    private final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public UserController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @PostMapping(path = "/create")
    public UserResponseDto createNewUser(@RequestBody @Valid UserCreationDto userCreationDto) {
        User user = this.convertToUserEntity(userCreationDto);
        return this.convertToUserResponseDto(this.userService.createUser(user));
    }

    @GetMapping(path = "/search")
    public ArrayList<UserResponseDto> searchUsers(
            @RequestParam(value = "id", required = false) Integer id,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "firstName", required = false) String firstName,
            @RequestParam(value = "lastName", required = false) String lastName) {

        return this.userService.search(id, username, email, firstName, lastName)
                .stream()
                .map((user) -> (this.convertToUserResponseDto(user)))
                .collect(Collectors.toCollection(ArrayList::new));
    }


    // Dto <-> Entity Conversions

    private  UserResponseDto convertToUserResponseDto(User user) {
        UserResponseDto userResponseDto = modelMapper.map(user, UserResponseDto.class);
        return userResponseDto;
    }


    private User convertToUserEntity(UserCreationDto userCreationDto) {
        User user = modelMapper.map(userCreationDto, User.class);

        user.setPassword(BCrypt.hashpw(userCreationDto.getPassword(), BCrypt.gensalt()));
        user.setActive(true);
        user.setRole(this.roleService.findByName("USER"));

        return user;
    }
}
