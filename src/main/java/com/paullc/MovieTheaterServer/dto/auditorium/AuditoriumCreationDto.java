package com.paullc.MovieTheaterServer.dto.auditorium;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AuditoriumCreationDto {

    @JsonProperty("quantity")
    @NotNull(message = "quantity is required")
    private Integer quantity;

}
