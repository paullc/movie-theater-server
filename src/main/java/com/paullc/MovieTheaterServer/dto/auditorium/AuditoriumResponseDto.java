package com.paullc.MovieTheaterServer.dto.auditorium;

import lombok.Data;

@Data
public class AuditoriumResponseDto {

    private Integer id;

}
