package com.paullc.MovieTheaterServer.dto.film;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class FilmCreationDto {

    @NotBlank(message = "title is required")
    @JsonProperty("title")
    private String title;

    @NotBlank(message = "director is required")
    @JsonProperty("director")
    private String director;

    @NotNull(message = "releaseYear is required")
    @JsonProperty("releaseYear")
    private Integer releaseYear;

    @NotNull(message = "duration is required")
    @JsonProperty("duration")
    private Integer duration;

    @NotBlank(message = "mpaaRating is required")
    @JsonProperty("mpaaRating")
    private String mpaaRating;


}
