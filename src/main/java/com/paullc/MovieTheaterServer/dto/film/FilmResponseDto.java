package com.paullc.MovieTheaterServer.dto.film;

import lombok.Data;

@Data
public class FilmResponseDto {


    private Integer id;

    private String title;

    private String director;

    private Integer releaseYear;

    private Integer duration;

    private String mpaaRating;



}
