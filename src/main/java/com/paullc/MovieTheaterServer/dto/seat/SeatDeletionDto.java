package com.paullc.MovieTheaterServer.dto.seat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class SeatDeletionDto {

    @NotNull(message = "seatNumber is required")
    @JsonProperty("seatNumber")
    private Integer seatNumber;

    @NotBlank(message = "seatRow is required")
    @JsonProperty("seatRow")
    private String seatRow;

    @NotNull(message = "auditoriumId is required")
    @JsonProperty("auditoriumId")
    private Integer auditoriumId;
}
