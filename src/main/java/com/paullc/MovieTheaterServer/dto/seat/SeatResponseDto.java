package com.paullc.MovieTheaterServer.dto.seat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SeatResponseDto {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("seatNumber")
    private Integer seatNumber;

    @JsonProperty("seatRow")
    private String seatRow;

    @JsonProperty("auditoriumId")
    private Integer auditoriumId;

}
