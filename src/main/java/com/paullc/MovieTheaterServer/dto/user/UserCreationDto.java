package com.paullc.MovieTheaterServer.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UserCreationDto {


    @NotBlank(message = "username is mandatory")
    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    @NotBlank(message = "password is mandatory")
    private String password;

    @Email(message = "email must be valid")
    @NotBlank(message = "email is mandatory")
    @JsonProperty("email")
    private String email;

    @NotBlank(message = "firstName is mandatory")
    @JsonProperty("firstName")
    private String firstName;

    @NotBlank(message = "lastName is mandatory")
    @JsonProperty("lastName")
    private String lastName;

}
