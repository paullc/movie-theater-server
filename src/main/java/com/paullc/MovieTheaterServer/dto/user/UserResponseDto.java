package com.paullc.MovieTheaterServer.dto.user;

import lombok.Data;

@Data
public class UserResponseDto {


    private Integer id;
    private String username;
    private String email;

}
