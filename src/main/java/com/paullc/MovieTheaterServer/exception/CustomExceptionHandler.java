package com.paullc.MovieTheaterServer.exception;

import com.paullc.MovieTheaterServer.exception.film.ExistingFilmException;
import com.paullc.MovieTheaterServer.exception.role.ExistingRoleNameException;
import com.paullc.MovieTheaterServer.exception.role.RoleIdNotFoundException;
import com.paullc.MovieTheaterServer.exception.role.RoleNameNotFoundException;
import com.paullc.MovieTheaterServer.exception.seat.ExistingSeatException;
import com.paullc.MovieTheaterServer.exception.user.ExistingUserEmailException;
import com.paullc.MovieTheaterServer.exception.user.ExistingUsernameException;
import com.paullc.MovieTheaterServer.exception.user.UserIdNotFoundException;
import com.paullc.MovieTheaterServer.exception.user.UsernameNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    // User

    @ExceptionHandler({ ExistingUsernameException.class, ExistingUserEmailException.class})
    public ResponseEntity<Object> handleExistingUser(
            RuntimeException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());


        return new ResponseEntity<>(body, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({ UserIdNotFoundException.class, UsernameNotFoundException.class})
    public ResponseEntity<Object> handleUserNotFound(
            RuntimeException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());


        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }

    // Role
    @ExceptionHandler({ExistingRoleNameException.class})
    public ResponseEntity<Object> handleExistingRole(
            RuntimeException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());


        return new ResponseEntity<>(body, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({RoleNameNotFoundException.class, RoleIdNotFoundException.class})
    public ResponseEntity<Object> handleRoleNotFound(
            RuntimeException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());


        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }

    // Film

    @ExceptionHandler({ExistingFilmException.class})
    public ResponseEntity<Object> handleExistingFilm(
            RuntimeException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());


        return new ResponseEntity<>(body, HttpStatus.CONFLICT);
    }

    // Seat

    @ExceptionHandler({ExistingSeatException.class})
    public ResponseEntity<Object> handleExistingSeat(
            RuntimeException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());


        return new ResponseEntity<>(body, HttpStatus.CONFLICT);
    }




    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("status", status.value());

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        body.put("errors", errors);

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }
}
