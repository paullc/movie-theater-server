package com.paullc.MovieTheaterServer.exception.auditorium;

public class AuditoriumIdNotFoundException extends RuntimeException{

    public AuditoriumIdNotFoundException(Integer id) {
        super(String.format("Auditorium with id='%d' was not found.", id));
    }
}
