package com.paullc.MovieTheaterServer.exception.auditorium;

public class ExistingAuditoriumException extends RuntimeException{

    public ExistingAuditoriumException(String message) {
        super(String.format("Auditorium exists"));
    }
}
