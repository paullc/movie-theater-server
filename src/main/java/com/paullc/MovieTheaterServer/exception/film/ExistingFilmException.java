package com.paullc.MovieTheaterServer.exception.film;

public class ExistingFilmException extends RuntimeException{
    public ExistingFilmException(String title) {
        super(String.format("Film with title='%s' is an exact duplicate of a film in this database.", title));
    }

}
