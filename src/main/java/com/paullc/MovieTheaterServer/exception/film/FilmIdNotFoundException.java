package com.paullc.MovieTheaterServer.exception.film;

public class FilmIdNotFoundException extends RuntimeException{

    public FilmIdNotFoundException(Integer id) {
        super(String.format("Film with id='%d' was not found.", id));
    }
}
