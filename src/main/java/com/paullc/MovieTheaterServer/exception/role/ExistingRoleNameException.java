package com.paullc.MovieTheaterServer.exception.role;

public class ExistingRoleNameException extends RuntimeException{

    public ExistingRoleNameException(String name) {
        super(String.format("Role with name='%s' already exists.", name));
    }
}
