package com.paullc.MovieTheaterServer.exception.role;

public class RoleIdNotFoundException extends RuntimeException{

    public RoleIdNotFoundException(Integer id) {
        super(String.format("Role with id=%d was not found.", id));
    }
}
