package com.paullc.MovieTheaterServer.exception.role;

public class RoleNameNotFoundException extends RuntimeException{

    public RoleNameNotFoundException(String name) {
        super(String.format("Role with name=%s was not found.", name));
    }
}
