package com.paullc.MovieTheaterServer.exception.seat;

public class ExistingSeatException extends RuntimeException{
    public ExistingSeatException(String seatRow, Integer seatNumber, Integer auditoriumId) {

        super(String.format("Seat with seatRow='%s', seatNumber=%d, and auditoriumId=%d is " +
                "an exact duplicate of a seat in this database.", seatRow, seatNumber, auditoriumId));
    }

}
