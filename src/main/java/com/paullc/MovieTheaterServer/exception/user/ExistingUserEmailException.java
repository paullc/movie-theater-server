package com.paullc.MovieTheaterServer.exception.user;

public class ExistingUserEmailException extends RuntimeException{

    public ExistingUserEmailException(String email) {
        super(String.format("email='%s' is already in use.", email));
    }
}