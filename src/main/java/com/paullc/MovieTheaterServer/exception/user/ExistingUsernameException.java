package com.paullc.MovieTheaterServer.exception.user;

public class ExistingUsernameException extends RuntimeException {

    public ExistingUsernameException(String username) {
        super(String.format("username='%s' is already in use.", username));
    }


}