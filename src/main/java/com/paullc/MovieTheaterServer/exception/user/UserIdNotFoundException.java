package com.paullc.MovieTheaterServer.exception.user;

public class UserIdNotFoundException extends RuntimeException{

    public UserIdNotFoundException(Integer id) {
        super(String.format("User with id=%d was not found.", id));
    }
}