package com.paullc.MovieTheaterServer.model;


import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Auditorium {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(mappedBy = "auditorium")
    @ToString.Exclude
    private Set<Seat> seats;

    @OneToMany(mappedBy = "auditorium")
    @ToString.Exclude
    private Set<Screening> screenings;

}
