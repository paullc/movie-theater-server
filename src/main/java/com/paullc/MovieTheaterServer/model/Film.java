package com.paullc.MovieTheaterServer.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private String director;

    private Integer releaseYear;

    private Integer duration;

    private String mpaaRating;

    @OneToMany(mappedBy = "film")
    @ToString.Exclude
    private Set<Screening> screening;

}
