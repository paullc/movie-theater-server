package com.paullc.MovieTheaterServer.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private User user;

    @ManyToOne
    @ToString.Exclude
    private Screening screening;

    @ManyToMany
    @ToString.Exclude
    private Set<Seat> seat;

}
