package com.paullc.MovieTheaterServer.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Data
public class Screening {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @Temporal(TemporalType.TIME)
    @JsonFormat(pattern = "HH:mm aa")
    private Date time;

    @ManyToOne
    private Film film;

    @ManyToOne
    @ToString.Exclude
    private Auditorium auditorium;

    @OneToMany(mappedBy = "screening")
    @ToString.Exclude
    private Set<Reservation> reservation;

}
