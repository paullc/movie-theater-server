package com.paullc.MovieTheaterServer.model;


import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    private String seatRow;

    private Integer seatNumber;

    @ManyToOne
    @ToString.Exclude
    private Auditorium auditorium;

    @ManyToMany(mappedBy = "seat")
    @ToString.Exclude
    private Set<Reservation> reservation;

}
