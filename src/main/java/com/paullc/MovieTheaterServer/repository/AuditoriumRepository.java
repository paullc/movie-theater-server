package com.paullc.MovieTheaterServer.repository;

import com.paullc.MovieTheaterServer.model.Auditorium;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface AuditoriumRepository extends CrudRepository<Auditorium, Integer> {

    @Query("SELECT a FROM Auditorium a WHERE (:id is null or a.id = :id)")
    ArrayList<Auditorium> searchAuditoriums(Integer id);
}
