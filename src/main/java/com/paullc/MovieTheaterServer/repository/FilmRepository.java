package com.paullc.MovieTheaterServer.repository;

import com.paullc.MovieTheaterServer.model.Film;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface FilmRepository extends CrudRepository<Film, Integer> {

    @Query("SELECT f FROM Film f WHERE (:id is null or f.id = :id) and " +
            "(:title is null or f.title = :title) and " +
            "(:duration is null or f.duration = :duration) and" +
            "(:director is null or f.director = :director) and " +
            "(:mpaaRating is null or f.mpaaRating = :mpaaRating)")
    ArrayList<Film> searchFilms(Integer id, String title, Integer duration, String director, String mpaaRating);
}
