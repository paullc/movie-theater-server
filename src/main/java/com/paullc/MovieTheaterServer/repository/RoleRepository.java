package com.paullc.MovieTheaterServer.repository;

import com.paullc.MovieTheaterServer.model.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {

    @Query("SELECT r FROM Role r WHERE (:id is null or r.id = :id) and " +
            "(:name is null or r.name = :name)")
    ArrayList<Role> search(Integer id, String name);

    @Query("SELECT r FROM Role r WHERE r.id = :id")
    Optional<Role> findById(Integer id);

    @Query("SELECT r FROM Role r WHERE r.name = :name")
    Optional<Role> findByName(String name);

}
