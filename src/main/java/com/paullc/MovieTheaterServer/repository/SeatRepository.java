package com.paullc.MovieTheaterServer.repository;

import com.paullc.MovieTheaterServer.model.Seat;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Repository
public interface SeatRepository extends CrudRepository<Seat, Integer> {


    @Query("SELECT s FROM Seat s WHERE (:id is null or s.id = :id) and " +
            "(:seatRow is null or s.seatRow = :seatRow) and " +
            "(:seatNumber is null or s.seatNumber = :seatNumber) and" +
            "(:auditoriumId is null or s.auditorium.id = :auditoriumId)")
    ArrayList<Seat> searchSeats(Integer id, String seatRow, Integer seatNumber, Integer auditoriumId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Seat s WHERE (s.seatRow = :seatRow) and " +
            "(s.seatNumber = :seatNumber) and " +
            "(s.auditorium.id = :auditoriumId)")
    void deleteSeat(String seatRow, Integer seatNumber, Integer auditoriumId);

}
