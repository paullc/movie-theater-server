package com.paullc.MovieTheaterServer.repository;


import com.paullc.MovieTheaterServer.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    @Query("SELECT u FROM User u WHERE (:id is null or u.id = :id) and " +
            "(:username is null or u.username = :username) and " +
            "(:email is null or u.email = :email) and " +
            "(:firstName is null or u.firstName = :firstName) and " +
            "(:lastName is null or u.lastName = :lastName)")
    ArrayList<User> search(Integer id, String username, String email, String firstName, String lastName);

    @Query("SELECT u FROM User u WHERE u.username = :username")
    Optional<User> findByUsername(String username);

    @Query("SELECT u FROM User u WHERE u.email = :email")
    Optional<User> findByEmail(String email);
}
