package com.paullc.MovieTheaterServer.service;

import com.paullc.MovieTheaterServer.exception.auditorium.AuditoriumIdNotFoundException;
import com.paullc.MovieTheaterServer.model.Auditorium;
import com.paullc.MovieTheaterServer.repository.AuditoriumRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class AuditoriumService {

    private final AuditoriumRepository repository;

    public AuditoriumService(AuditoriumRepository repository) {
        this.repository = repository;
    }

    public Auditorium findById(Integer id) {
        return this.repository
                .findById(id)
                .orElseThrow(() -> new AuditoriumIdNotFoundException(id));
    }

    public Auditorium createAuditorium(Auditorium auditorium) {
        return this.repository.save(auditorium);
    }

    public ArrayList<Auditorium> createAuditoriums(ArrayList<Auditorium> auditoriums) {
        return (ArrayList<Auditorium>) this.repository.saveAll(auditoriums);
    }

    public ArrayList<Auditorium> searchAuditoriums(Integer id) {
        return this.repository.searchAuditoriums(id);
    }
}
