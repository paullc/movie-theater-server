package com.paullc.MovieTheaterServer.service;

import com.paullc.MovieTheaterServer.dto.user.CustomUserDetails;
import com.paullc.MovieTheaterServer.dto.user.UserResponseDto;
import com.paullc.MovieTheaterServer.exception.user.UsernameNotFoundException;
import com.paullc.MovieTheaterServer.model.User;
import com.paullc.MovieTheaterServer.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper = new ModelMapper();


    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.convertToUserDetails(
                this.userRepository
                        .findByUsername(username)
                        .orElseThrow(() -> new UsernameNotFoundException(username)));
    }

    private CustomUserDetails convertToUserDetails(User user) {
        CustomUserDetails userDetails = modelMapper.map(user, CustomUserDetails.class);
        userDetails.setAuthorities(Set.of(new SimpleGrantedAuthority(user.getRole().getName())));
        return userDetails;
    }
}
