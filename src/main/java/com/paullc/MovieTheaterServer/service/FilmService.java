package com.paullc.MovieTheaterServer.service;


import com.paullc.MovieTheaterServer.exception.film.ExistingFilmException;
import com.paullc.MovieTheaterServer.exception.film.FilmIdNotFoundException;
import com.paullc.MovieTheaterServer.model.Film;
import com.paullc.MovieTheaterServer.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class FilmService {

    private final FilmRepository repository;

    @Autowired
    public FilmService(FilmRepository repository) {
        this.repository = repository;
    }

    public Film createFilm(Film film) {

        if (this.searchFilms(null,
                film.getTitle(), film.getDuration(),
                film.getDirector(),
                film.getMpaaRating()).isEmpty()) {

            return this.repository.save(film);

        }
        else {
            throw new ExistingFilmException(film.getTitle());
        }
    }

    public ArrayList<Film> createFilms(ArrayList<Film> films) {

        films.forEach((film) -> {
            if (!this.searchFilms(film.getId(),
                    film.getTitle(), film.getDuration(),
                    film.getDirector(),
                    film.getMpaaRating()).isEmpty()) {

                throw new ExistingFilmException(film.getTitle());

            }
        });

        return (ArrayList<Film>) this.repository.saveAll(films);

    }

    public ArrayList<Film> searchFilms(Integer id, String title, Integer duration, String director, String mpaaRating) {
        return this.repository.searchFilms(id, title, duration, director, mpaaRating);
    }

    public Film findById(Integer id) {
        return this.repository
                .findById(id)
                .orElseThrow(() -> new FilmIdNotFoundException(id));
    }
}
