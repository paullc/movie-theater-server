package com.paullc.MovieTheaterServer.service;

import com.paullc.MovieTheaterServer.exception.role.ExistingRoleNameException;
import com.paullc.MovieTheaterServer.exception.role.RoleIdNotFoundException;
import com.paullc.MovieTheaterServer.exception.role.RoleNameNotFoundException;
import com.paullc.MovieTheaterServer.model.Role;
import com.paullc.MovieTheaterServer.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class RoleService {

    private final RoleRepository repository;

    @Autowired
    public RoleService(RoleRepository repository) {
        this.repository = repository;
    }

    public Role createRole(Role role) {

        if (this.repository.findByName(role.getName()).isPresent()) {
            throw new ExistingRoleNameException(role.getName());
        }

        return this.repository.save(role);

    }

    public ArrayList<Role> search(Integer id, String username) {

        return this.repository.search(id, username);
    }

    public Role findById(Integer id) {
        return this.repository
                .findById(id)
                .orElseThrow(() -> new RoleIdNotFoundException(id));
    }

    public Role findByName(String name) {
        return this.repository
                .findByName(name)
                .orElseThrow(() -> new RoleNameNotFoundException(name));
    }

}
