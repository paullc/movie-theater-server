package com.paullc.MovieTheaterServer.service;

import com.paullc.MovieTheaterServer.exception.seat.ExistingSeatException;
import com.paullc.MovieTheaterServer.model.Seat;
import com.paullc.MovieTheaterServer.repository.SeatRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class SeatService {

    private final SeatRepository repository;

    public SeatService(SeatRepository repository) {
        this.repository = repository;
    }

    public ArrayList<Seat> createSeats(ArrayList<Seat> seats) {


        seats.forEach((seat) -> {
            if (!this.searchSeats(null,
                    seat.getSeatRow(),
                    seat.getSeatNumber(),
                    seat.getAuditorium().getId()).isEmpty()) {

                throw new ExistingSeatException(seat.getSeatRow(), seat.getSeatNumber(), seat.getAuditorium().getId());

            }
        });

        return (ArrayList<Seat>) this.repository.saveAll(seats);

    }

    public void deleteSeat(String seatRow, Integer seatNumber, Integer auditoriumId) {
        this.repository.deleteSeat(seatRow, seatNumber, auditoriumId);

    }

    public void deleteSeatsById(ArrayList<Integer> ids) {
        this.repository.deleteAllById(ids);
    }

    public ArrayList<Seat> searchSeats(Integer id, String seatRow, Integer seatNumber, Integer auditoriumId) {
        return this.repository.searchSeats(id, seatRow, seatNumber, auditoriumId);
    }

}
