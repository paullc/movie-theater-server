package com.paullc.MovieTheaterServer.service;

import com.paullc.MovieTheaterServer.exception.user.ExistingUserEmailException;
import com.paullc.MovieTheaterServer.exception.user.ExistingUsernameException;
import com.paullc.MovieTheaterServer.exception.user.UserIdNotFoundException;
import com.paullc.MovieTheaterServer.exception.user.UsernameNotFoundException;
import com.paullc.MovieTheaterServer.model.User;
import com.paullc.MovieTheaterServer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService {

    private final UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public User createUser(User user) {

        if (this.repository.findByUsername(user.getUsername()).isPresent()) {
            throw new ExistingUsernameException(user.getUsername());
        }

        if (this.repository.findByEmail(user.getEmail()).isPresent()) {
            throw new ExistingUserEmailException(user.getEmail());
        }
        return this.repository.save(user);
    }

    public ArrayList<User> search(Integer id, String username, String email, String firstName, String lastName) {

        return this.repository.search(id, username, email, firstName, lastName);
    }


    public User findById(Integer id) {
        return this.repository
                .findById(id)
                .orElseThrow(() -> new UserIdNotFoundException(id));
    }

    public User findByUsername(String username) {
        return this.repository
                .findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }
}
